package com.zuitt.batch232;

public class Main {

    public static void main(String[] args) {

        // Phonebook 1
        Phonebook phonebook = new Phonebook();
            // Contact 1
            Contact john = new Contact();
            john.setName("John Doe");
            john.setNumbers("+639152468596");
            john.setAddress("my home in Quezon City");
            // Contact 2
            Contact jane = new Contact();
            jane.setName("Jane Doe");
            // jane.setNumbers("+639162148573"); -> checking if it will catch an error if there is no number set
            jane.setAddress("my home in Caloocan City");
            // setting contacts
            phonebook.addContacts(john);
            phonebook.addContacts(jane);
            // print new contacts
            phonebook.displayPhoneBook();

        // Phonebook 2 -> Empty phonebook
        Phonebook phonebook2 = new Phonebook();
        phonebook2.displayPhoneBook(); // - > print an error if phonebook is empty!
    }

}

