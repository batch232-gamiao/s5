package com.zuitt.batch232;

import java.util.ArrayList;

public class Contact {

    String name;
    ArrayList<String> contactNumber = new ArrayList<>();  //-> I created the contactNumber as arraylist for practice in real life
    ArrayList<String> address = new ArrayList<>(); //-> I created the address as arraylist for practice in real life

    // Empty Constructor
    public Contact(){
    }

    // Parameterized Constructor
    public Contact (String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber.add(contactNumber);
        this.address.add(address);
    }

    // Getters
    public String getName() {
        return this.name;
    }
    public ArrayList<String> getNumbers() {
        return this.contactNumber;
    }
    public ArrayList<String> getAddress() {
        return this.address;
    }

    // Setters
    public void setName(String name) {
        this.name = name;
    }
    public void setNumbers(String contactNumber) {
        this.contactNumber.add(contactNumber);
    }
    public void setAddress(String address) {
        this.address.add(address);
    }
}


