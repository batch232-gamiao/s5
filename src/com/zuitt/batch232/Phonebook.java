package com.zuitt.batch232;

import java.util.ArrayList;

public class Phonebook {

    ArrayList<Contact> contacts = new ArrayList<>();

    // Empty Constructor
    public Phonebook(){
    }

    // Parameterized Constructor --> not used
    public Phonebook (Contact contact) {
        this.contacts.add(contact);
    }

    // Getter
    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    // Setter
    public void addContacts(Contact contact) {
        this.contacts.add(contact);
    }

    public void displayPhoneBook(){

        String result = ""; // -> display results per line

        if (this.contacts.size() == 0) {
            result = "Error: Phonebook is empty!";
        }
        else {
            for (Contact contact: contacts) {
                result += result.length() == 0 ? "" : "\n";


                result += "\n" + contact.getName() + "\n" + "-".repeat(20);

                result += "\n" + contact.getName() + " has the following registered number:";

                if (contact.getNumbers().size() == 0) {
                    result += "\n" + "Error: There is no registered number!";
                }
                else {
                    for (String number: contact.getNumbers()) {
                        result += "\n" + number;
                    }
                }

                result += "\n" + "-".repeat(20);

                result += "\n" + contact.getName() + " has the following registered address:";

                if (contact.getAddress().size() == 0){
                    result += "\n" + "Error: There is no registered address!";
                }
                else {
                    for (String address: contact.getAddress()) {
                        result += "\n" + address;
                    }
                }
                result += "\n" + " ";
            }
        }
        System.out.println(result);
    }
}

